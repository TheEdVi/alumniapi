<?php
/**
 * Created by Edoardo Viviani
 * Date: 08/12/16
 * Copyright (C) 2016
 */

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

class AuthHelper{

    private $secret;
    private $token;
    private $authed = false;

    public function __construct($secret) {
        $this->secret = $secret;

        // check if current request is authenticated
        if(isset($_SERVER["HTTP_AUTHENTICATION"])){
            try {
                $this->token = (new Parser())->parse((string)$_SERVER["HTTP_AUTHENTICATION"]);
            }catch (Exception $e){
                return false;
            }

            $this->authed = $this->token->validate(new ValidationData()) && $this->token->verify(new Sha256(), $this->secret);
        }
    }

    // create a new token
    public function getToken($username){
        return (string) (new Builder())
        ->setIssuedAt(time())
        //->setExpiration(time() + 3600)
        ->set('username', $username)
        ->sign(new Sha256(), $this->secret)
        ->getToken();
    }

    public function isAuthed(){
        return $this->authed;
    }

    public function getUsername(){
        if(!$this->authed)
            return null;

        return $this->token->getClaim('username');
    }

}