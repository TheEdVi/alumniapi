<?php
/**
 * Created by Edoardo Viviani
 * Date: 04/12/16
 * Copyright (C) 2016
 */

class RestHelper {
    static private $put = [];
    static private $delete = [];
    static private $parsed = false;

    private static function parse() {
        if(self::$parsed) return;
        self::$parsed = true;

        $vars = [];
        parse_str(file_get_contents('php://input'), $vars);
        $_REQUEST = array_merge($vars, $_REQUEST);

        switch ($_SERVER['REQUEST_METHOD']) {
            case 'PUT':
                self::$put = $vars;
                break;
            case 'DELETE':
                self::$delete = $vars;
        }
    }

    public static function _DELETE() {
        self::parse();
        return self::$delete;
    }

    public static function _PUT() {
        self::parse();
        return self::$put;
    }
}