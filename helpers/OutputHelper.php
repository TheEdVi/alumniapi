<?php
/**
 * Created by Edoardo Viviani
 * Date: 08/12/16
 * Copyright (C) 2016
 */

class OutputHelper{

    // use this function to standardize API output
    public static function out($content, $status = 200, $options = JSON_PRETTY_PRINT){
        http_response_code($status);
        header('Content-Type: application/json');

        echo json_encode([
            'status' => $status,
            $status != 200 ? 'error' : 'data' => $content
        ], $options);
    }

}