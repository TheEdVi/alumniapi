<?php
/**
 * Created by Edoardo Viviani
 * Date: 04/12/16
 * Copyright (C) 2016
 */

require_once __DIR__ . '/Controller.php';

class IndexController extends Controller {

    public function index(){
        $this->out(['message'=> 'Welcome to AlumniAPI v1', 'server_datetime'=> (new DateTime())->format('Y-m-d H:i:s')]);
    }

}