<?php
/**
 * Created by Edoardo Viviani
 * Date: 04/12/16
 * Copyright (C) 2016
 */

require_once __DIR__ . '/Controller.php';
require_once __DIR__ . '/../helpers/RestHelper.php';

class EventController extends Controller{

    protected $requireAuth = true;

    /* GET /events
     * Body: (nothing)
     *
     * Returns all events in database (or an empty array if none is present)
     */
    public function getAllEvents(){ //GET
        $res = $this->db->query('SELECT odg, date FROM events ORDER BY date');

        // Query returns 'false' if there is an error
        if(!$res) {
            $this->out('DB error', 500);
            exit();
        }

        $events = [];
        while($event = mysqli_fetch_assoc($res))
            $events[] = $event;

        $this->out($events);
    }

    /* GET /events/{date}
     * Body: (nothing)
     *
     * Returns event with corresponding date or 404 if the event is not found
     */
    public function getEvent($date){ //GET
        $date = DateTime::createFromFormat('Y-m-d', $date);

        // Check if input date is in a correct format (DateTime returns false if not)
        if(!$date){
            $this->out('Invalid date', 400);
            exit();
        }

        $res = $this->db->query('SELECT odg, date FROM events WHERE date = \'' . $date->format('Y-m-d') . '\'');

        // Query returns 'false' if there is an error
        if(!$res) {
            $this->out('DB error', 500);
            exit();
        }

        $event = mysqli_fetch_assoc($res);

        if($event)
            $this->out($event);
        else
            $this->out('Event not found', 404);
    }

    /* POST /events
     * Body: date, odg
     *
     * Creates a new event or returns 400 if an event exist with same date
     */
    public function createEvent(){ //POST
        if(!(isset($_POST['date']) && isset($_POST['odg']))) { // check if there are requirements
            $this->out('Missing arguments', 400);
            exit();
        }

        $date = DateTime::createFromFormat('Y-m-d', $_POST['date']);

        if(!$date){     // check if date is valid
            $this->out('Invalid date', 400);
            exit();
        }

        $res = $this->db->query("SELECT COUNT(*) FROM events WHERE date = '{$date->format('Y-m-d')}'");

        $exists = mysqli_fetch_array($res);

        if($exists[0]){    // check if event exists
            $this->out('An event exists at given date', 400);
            exit();
        }

        $res = $this->db->query('INSERT INTO events (odg, date) VALUES (\'' . mysqli_escape_string($this->db, $_POST['odg']). '\', \'' . $date->format('Y-m-d') . '\')');

        if($res)
            $this->out('Event created', 200);
        else
            $this->out('DB error', 500);
    }

    /* POST /events/{date}
     * Body: (empty)
     *
     * Deletes an event if exists and only if there are no firms
     */
    public function deleteEvent($date){ //DELETE
        $date = DateTime::createFromFormat('Y-m-d', $date);

        // check if events exists
        $res = $this->db->query("SELECT COUNT(*) FROM events WHERE date = '{$date->format('Y-m-d')}'");

        $exists = mysqli_fetch_array($res);

        if(!$exists[0]){    // check if event exists
            $this->out('Event not found', 404);
            exit();
        }

        // check if there are already firms at the event and prevent deleting
        $res = $this->db->query("SELECT count(*) FROM events, entries WHERE events.event_id = entries.event_id and date = '{$date->format('Y-m-d')}'");

        $exists = mysqli_fetch_array($res);

        if($exists[0]){    // check if event exists
            $this->out('There are already firms for this event', 400);
            exit();
        }

        $res = $this->db->query("DELETE FROM events WHERE date = '{$date->format('Y-m-d')}'");

        if($res)
            $this->out('Event deleted', 200);
        else
            $this->out('DB error', 500);
    }


    /* POST /events/{date}
     * Body: new_date (optional), new_odg (optional)
     *
     * Edits event date and/or odg
     */
    public function editEvent($date){ //PUT
        $_PUT = RestHelper::_PUT(); // parse PUT body

        $date = DateTime::createFromFormat('Y-m-d', $date);
        if(!$date){ // validate old date
            $this->out('Invalid date', 400);
            exit();
        }
        $date = $date->format('Y-m-d');

        $res = $this->db->query("SELECT COUNT(*) FROM events WHERE date = '{$date}'");
        $exists = mysqli_fetch_array($res);
        if(!$exists[0]){    // check if event exists
            $this->out('Event not found', 404);
            exit();
        }

        if(isset($_PUT['new_date'])) { // validate new date
            $new_date = DateTime::createFromFormat('Y-m-d', $_PUT['new_date']);

            if(!$new_date){
                $this->out('Invalid new_date', 400);
                exit();
            }
        }else
            $new_date = null;

        if(isset($_PUT['new_odg']))
            $new_odg = mysqli_escape_string($this->db, $_PUT['new_odg']);
        else
            $new_odg = null;

        if($new_odg && $new_date)
            $res = $this->db->query("UPDATE events SET date = '{$new_date->format('Y-m-d')}', odg = '$new_odg' WHERE date = '{$date}'");
        else if($new_odg)
            $res = $this->db->query("UPDATE events SET odg = '$new_odg' WHERE date = '{$date}'");
        else if($new_date)
            $res = $this->db->query("UPDATE events SET date = '{$new_date->format('Y-m-d')}' WHERE date = '{$date}'");

        if($new_odg || $new_date)
            if($res)
                $this->out('Event updated');
            else
                $this->out('DB error', 500);
        else
            $this->out('Nothing to update', 400);
    }

}