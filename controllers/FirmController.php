<?php
/**
 * Created by Edoardo Viviani
 * Date: 04/12/16
 * Copyright (C) 2016
 */

require_once __DIR__ . '/Controller.php';

class FirmController extends Controller{

    protected $requireAuth = true;
    
    public function getAllFirms($email){ //GET
        $email = mysqli_escape_string($this->db, $email);

        $res = $this->db->query("SELECT students.name, students.surname, students.email, events.date, events.odg, entries.firm_date, entries.counterfirm_date
                                 FROM students, events, entries 
                                 WHERE entries.student_id = students.student_id and events.event_id = entries.event_id and students.email = '$email'");

        $firms = [];
        while($firm = mysqli_fetch_assoc($res))
            $firms[] = $firm;

        $this->out($firms);
    }

    public function getFirm($email, $date){ // GET
        $email = mysqli_escape_string($this->db, $email);

        $res = $this->db->query("SELECT students.name, students.surname, students.email, events.date, events.odg, entries.firm_date, entries.counterfirm_date
                                 FROM students, events, entries 
                                 WHERE entries.student_id = students.student_id and events.event_id = entries.event_id and students.email = '$email' and events.date = '$date'");

        $firm = mysqli_fetch_assoc($res);
        if($firm)
            $this->out($firm);
        else
            $this->out('Firm not found', 404);
    }
    
    public function createFirm($email, $action){ //POST
        $email = mysqli_escape_string($this->db, $email);
        $date = date('Y-m-d');
        $datetime = date('Y-m-d H-i-s');

        // Check if there are events today
        $res = $this->db->query("SELECT COUNT(*) FROM events WHERE date = '$date'");
        $exists = mysqli_fetch_array($res)[0];
        if(!$exists){
            $this->out('There are no events today', 400);
            exit();
        }

        // Check if student exists
        $res = $this->db->query("SELECT COUNT(*) FROM students WHERE email = '$email'");
        $exists = mysqli_fetch_array($res)[0];
        if(!$exists){
            $this->out('Student does not exist', 404);
            exit();
        }

        // check if firm exists (needed to avoid 2 firms of the same student and the inserition of counterfirms without firms)
        $res = $this->db->query("SELECT entries.firm_date FROM students, entries, events 
                                 WHERE entries.student_id = students.student_id and events.event_id = entries.event_id and students.email = '$email' and events.date = '$date'");
        $entry_exists = mysqli_fetch_assoc($res);

        switch($action){
            case 'firm':

                if($entry_exists){
                    $this->out('Firm exists', 400);
                    exit();
                }

                $res = $this->db->query("INSERT INTO entries (event_id, student_id, firm_date)
                                         SELECT events.event_id, students.student_id, '$datetime'
                                         FROM events, students
                                         WHERE events.date = '$date' and students.email = '$email'");

                if($res)
                    $this->out('Firm created');
                else
                    $this->out('DB error', 500);

                break;
            case 'counterfirm':

                if($entry_exists){
                    $this->out('Firm does not exist. Counterfirm not created', 400);
                    exit();
                }

                $res = $this->db->query("SELECT entries.counterfirm_date FROM students, entries, events 
                                         WHERE entries.student_id = students.student_id
                                         and events.event_id = entries.event_id
                                         and students.email = '$email'
                                         and events.date = '$date'");
                $counterfirm_exists = mysqli_fetch_array($res)[0]; // take first and only value (counterfirm_date)
                if($counterfirm_exists){
                    $this->out('Firm exists', 400);
                    exit();
                }
        }
    }
    
    public function deleteFirm($email, $action){ //DELETE
        $this->out('Not implemented',  501);
    }
    
    public function editFirm($email, $action){ //PUT
        $this->out('Not implemented',  501);
    }
    
}