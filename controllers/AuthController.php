<?php
/**
 * Created by Edoardo Viviani
 * Date: 08/12/16
 * Copyright (C) 2016
 */

require_once __DIR__ . '/Controller.php';

class AuthController extends Controller {


    /* POST /login
     * Body: username, password
     *
     * Checks if the user exists in DB and returns JWT token
     */
    public function doLogin(){
        if(!isset($_POST['username']) || !isset($_POST['password'])){
            $this->out('Missing parameters', 400);
            exit();
        }

        $username = mysqli_escape_string($this->db, $_POST['username']);
        $password = md5($_POST['password']);

        $res = $this->db->query("SELECT COUNT(*) FROM users WHERE username = '$username' and password = '$password'");

        if(!$res){
            $this->out('DB error', 500);
            exit();
        }

        $exists = mysqli_fetch_array($res)[0];

        if($exists)
            $this->out(['token' => $this->auth->getToken('EdVi')]);
        else
            $this->out('Username or password incorrect', 401);
    }

}