<?php
/**
 * Created by Edoardo Viviani
 * Date: 04/12/16
 * Copyright (C) 2016
 */

require_once __DIR__ . '/Controller.php';

class StudentController extends Controller{

    protected $requireAuth = true;

    public function getStudent($email){ //GET
        $email = mysqli_escape_string($this->db, $email);

        $res = $this->db->query("SELECT name, surname, class, email FROM students WHERE email = '$email'");

        if($res)
            $this->out(mysqli_fetch_assoc($res));
        else
            $this->out('Student not found', 404);
    }
    
    public function createStudent(){ //POST
        if(!(isset($_POST['name']) && isset($_POST['surname']) && isset($_POST['class']) && isset($_POST['email']))){
            $this->out('Missing arguments', 400);
            exit();
        }

        $name = mysqli_escape_string($this->db, $_POST['name']);
        $surname = mysqli_escape_string($this->db, $_POST['surname']);
        $class = mysqli_escape_string($this->db, $_POST['class']);
        $email = mysqli_escape_string($this->db, $_POST['email']);

        $res = $this->db->query("SELECT COUNT(*) FROM students WHERE email = '$email'");

        $exists = mysqli_fetch_assoc($res)[0];
        if($exists){
            $this->out('Email already in database', 400);
            exit();
        }

        $res = $this->db->query("INSERT INTO students(name, surname, class, email), VALUES ('$name', '$surname', '$class', '$email')");

        if($res)
            $this->out('Student created', 200);
        else
            $this->out('DB error', 500);
    }
    
    public function deleteStudent($email){ //DELETE
        $email = mysqli_escape_string($this->db, $email);

        $res = $this->db->query("SELECT COUNT(*) FROM students WHERE email = '$email'");

        $exists = mysqli_fetch_assoc($res)[0];
        if(!$exists){
            $this->out('Student does not exist', 400);
            exit();
        }

        $res = $this->db->query("DELETE FROM students WHERE email = '$email'");

        if($res)
            $this->out('Student email', 200);
        else
            $this->out('DB error', 500);
    }
    
    public function editStudent($email){ //PUT
        $this->out('Not implemented',  501);
    }

}