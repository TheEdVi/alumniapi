<?php
/**
 * Created by Edoardo Viviani
 * Date: 04/12/16
 * Copyright (C) 2016
 */

require_once __DIR__ . '/../helpers/OutputHelper.php';
require_once __DIR__ . '/../helpers/AuthHelper.php';

class Controller{

    protected $requireAuth = false;
    protected $db;
    protected $auth;

    public function __construct(mysqli $db, AuthHelper $auth){
        $this->db = $db;
        $this->auth = $auth;

        if($this->requireAuth){
            if(!$this->auth->isAuthed()){
                $this->out('Unhautorized', 401);
                exit();
            }
        }
    }

    // use this function to standardize API output
    protected function out($content, $status = 200){
        OutputHelper::out($content, $status);
    }

}