-- SQL queries used by the API

-- select all firms and counterfirms given student email
SELECT students.name, students.surname, students.email, events.date, events.odg, entries.firm_date, entries.counterfirm_date
FROM students, events, entries
WHERE entries.student_id = students.student_id and events.event_id = entries.event_id and students.email = 'verdi.luigi@itismeucci.com'

-- select firm and counterfirm given student email and event date
SELECT students.name, students.surname, students.email, events.date, events.odg, entries.firm_date, entries.counterfirm_date
FROM students, events, entries
WHERE entries.student_id = students.student_id and events.event_id = entries.event_id and students.email = 'verdi.luigi@itismeucci.com' and events.date = '2017-01-12'

-- Insert firm in entries table
INSERT INTO entries (event_id, student_id, firm_date)
SELECT events.event_id, students.student_id, '2017-01-12 09:00:00' -- firm datetime
FROM events, students
WHERE events.date = '2017-01-12' and students.email = 'verdi.luigi@itismeucci.com' -- event date & student email

-- Check if there are firms at a given date
SELECT count(*) FROM events, entries
WHERE events.event_id = entries.event_id
and date = '2016-12-05'