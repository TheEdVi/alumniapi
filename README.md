AlumniAPI
=========

Sistema di gestione delle presenze

 [apidocs](APIdocs.md)

##### Requisiti

- PHP >=5.4
- MySQL
- Apache (+ mod_rewrite)

#### TODO

- :white_check_mark: DB
- :white_check_mark: Routing
- :white_check_mark: Auth (JWT)
- :white_check_mark: Gestione degli eventi
- :no_entry: Gestione degli studenti (75%, funzioni base)
- :no_entry: Gestione delle presenze (50%, funzioni base)
- :no_entry: Esportazione degli appelli per classe (0%)