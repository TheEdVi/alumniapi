<?php
/**
 * Created by Edoardo Viviani
 * Date: 03/12/16
 * Copyright (C) 2016
 */

// set timezone for datetimes in DB
date_default_timezone_set('Europe/Rome');

require_once __DIR__ . '/helpers/OutputHelper.php';
require_once __DIR__ . '/helpers/AuthHelper.php';

// load config file or return error and exit if the file is not found
if(file_exists('config.ini'))
    $config = parse_ini_file('config.ini', true);
else{
    OutputHelper::out('Configuration error', 500);
    exit();
}

// load composer's 3rd party files
require 'vendor/autoload.php';

###########
#   DB    #
###########

// connect to mysql
$db = new mysqli($config['DB']['host'], $config['DB']['username'], $config['DB']['password'], $config['DB']['name']);

// return error and exit if the connection to db goes wrong
if($db->connect_error){
    header('Content-Type: application/json');
    OutputHelper::out('Database connection error', 500);
    exit();
};

###########
#  Auth   #
###########

$auth = new AuthHelper($config['Auth']['secret']);

############
#  Router  #
############

// api router
$router = new AltoRouter();

// set router base path
if($config['URL']['base_path'])
    $router->setBasePath($config['URL']['base_path']);

$router->addMatchTypes(array('mail' => '([a-z0-9][-a-z0-9_\+\.]*[a-z0-9])@itismeucci.com'));
$router->addMatchTypes(array('date' => '\d{4}\-\d{1,2}-\d{1,2}')); // YYYY-MM-DD or YYYY-M-D

// Index
$router->map('GET',     '/', 'IndexController#index');

// Auth
$router->map('POST',     '/login', 'AuthController#doLogin');

// Events
$router->map('GET',     '/events',              'EventController#getAllEvents');
$router->map('GET',     '/events/[date:date]',  'EventController#getEvent');
$router->map('POST',    '/events',              'EventController#createEvent');
$router->map('DELETE',  '/events/[date:date]',  'EventController#deleteEvent');
$router->map('PUT',     '/events/[date:date]',  'EventController#editEvent');

// Students
$router->map('GET',     '/students/[mail:email]',    'StudentController#getStudent');
$router->map('POST',    '/students',                 'StudentController#createStudent');
$router->map('DELETE',  '/students/[mail:email]',    'StudentController#deleteStudent');
$router->map('PUT',     '/students/[mail:email]',    'StudentController#editStudent');

// Firms
$router->map('GET',     '/students/[mail:email]/firms',                      'FirmController#getAllFirms');
$router->map('GET',     '/students/[mail:email]/firms/[date:date]',          'FirmController#getFirm');
$router->map('POST',    '/students/[mail:email]/[firm|counterfirm:action]',  'FirmController#createFirm');
$router->map('DELETE',  '/students/[mail:email]/[firm|counterfirm:action]',  'FirmController#deleteFirm');
$router->map('PUT',     '/students/[mail:email]/[firm|counterfirm:action]',  'FirmController#editFirm');

// match current request url
$match = $router->match();

// call controller or throw errors
if($match) {
    if(is_callable($match['target'])) // if target is a function call it, otherwise search for controller
        call_user_func($match['target']);
    else {
        $target = explode('#', $match['target']); // Controller#method

        $controllerClass = $target[0];
        $controllerMethod = $target[1];

        $controllerFile = __DIR__ . '/controllers/' . $target[0] . '.php';

        if(file_exists($controllerFile)) { // validate controller
            require_once $controllerFile;

            if(class_exists($controllerClass) && is_callable([$controllerClass, $controllerMethod]))
                call_user_func_array([new $controllerClass($db, $auth), $controllerMethod], $match['params']); // call Controller#method
            else
                OutputHelper::out('Controller error', 500);
        }else
            OutputHelper::out('Controller not found', 500);
    }
} else
    OutputHelper::out('Not found', 404);