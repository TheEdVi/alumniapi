#AlumniAPI apidocs


###Auth
AlumniAPI usa token JWT per autenticare l'utente.
Per ottenerne uno è necessario effettuare una richiesta di tipo **POST** contenente i campi **username** e **password** all'url **/login**

È necessario inviare in ogni richiesta il token nell'header della richiesta nel campo **Authentication**

In caso contrario il server risponderà con un s.c. 401
```json
{
    "status": 401,
    "error": "Unhautorized"
}
```

#Routes
### GET /
Index dell'api, risponde sempre con lo s.c. 200, con un messaggio di benvenuto e la data corrente del server
```json
{
    "status": 200,
    "data": {
        "message": "Welcome to AlumniAPI v1",
        "server_datetime": "YYYY-MM-DD HH:MM:SS"
    }
}
```

### POST /login
Restituisce un token JWT se il login è andato a buon fine dati i campi **username** e **password**

## Gestione degli eventi
### GET /events
Restituisce tutti gli eventi presenti nel database
```json
{
  "status": 200,
  "data": [
    {
      "odg": "Ordine nel giorno",
      "date": "YYYY-MM-DD"
    },
    {
      "odg": "Ordine nel giorno",
      "date": "YYYY-MM-DD"
    }
  ]
}
```

### GET /events/{data}
Restituisce l'evento alla data richiesta (nel formato *YYYY-MM-DD*)
```json
{
  "status": 200,
  "data": {
    "odg": "Ordine nel giorno",
    "date": "YYYY-MM-DD"
  }
}
```
Oppure in caso l'evento non esista restituisce uno s.c. 404
```json
{
  "status": 404,
  "error": "Event not found"
}
```

### POST /events
Crea un nuovo evento se non ve ne è presente uno alla stessa data.
Il contenuto del body di tipo **application/x-www-form-urlencoded** deve contenere i campi **odg** e **date**
In caso di avvenuta creazione dell'evento restituisce s.c. 200
```json
{
  "status": 200,
  "data": "Event created"
}
```
Oppure, se l'evento è già esistente s.c. 400
```json
{
  "status": 400,
  "error": "An event exists at given date"
}
```

### DELETE /events/{date}
Elimina l'evento alla data specificata, restituisce s.c. 200 se l'azione è avvenuta
```json
{
  "status": 200,
  "data": "Event deleted"
}
```
Restituisce uno s.c 404 se l'evento non esiste
```json
{
  "status": 404,
  "error": "Event not found"
}
```
Restituisce uno s.c. 400 se ci sono delle firme a quell'evento
```json
{
  "status": 400,
  "error": "There are already firms for this event"
}
```
### PUT /events/{date}
Modifica **odg** e/o **date** di un evento  alla data specificata se nel body della richiesta sono presenti i campi **new_odg** o **new_date**
Restituisce un s.c. 200 se l'azione è avvenuta
```json
{
  "status": 200,
  "data": "Event updated"
}
```
Restituisce uno s.c 404 se l'evento non esiste
```json
{
  "status": 404,
  "error": "Event not found"
}
```
Restituisce uno s.c. 400 se non si è fornito ne **new_odg** ne **new_date**
```json
{
  "status": 400,
  "error": "Nothing to update"
}
```

## Gestione degli studenti
### GET /students/{email}
Restituisce lo studente data l'email d'istituto @itismeucci.com
```json
{
  "status": 200,
  "data": {
    "name": "Mario",
    "surname": "Rossi",
    "class": "1AIT",
    "email": "rossi.mario@itismeucci.com"
  }
}
```
Restituisce uno s.c. 404 se lo studente non è presente nel database
```json
{
  "status": 404,
  "error": "Student not found"
}
```

### POST /students
>TODO: documentation

### DELETE /students/{email}
>TODO: documentation

### PUT /students/{email}
>TODO: documentation

## Gersione delle firme
### GET /students/{email}/firms
Restituisce tutte le firme di un dato studente
```json
{
  "status": 200,
  "data": [
    {
      "name": "Mario",
      "surname": "Rossi",
      "email": "rossi.mario@itismeucci.com",
      "date": "YYYY-MM-DD",
      "odg": "Ordine del giorno",
      "firm_date": "YYYY-MM-DD HH-MM-SS",
      "counterfirm_date": "YYYY-MM-DD HH-MM-SS"
    },
    {
      "name": "Mario",
      "surname": "Rossi",
      "email": "rossi.mario@itismeucci.com",
      "date": "YYYY-MM-DD",
      "odg": "Ordine del giorno",
      "firm_date": "YYYY-MM-DD HH-MM-SS",
      "counterfirm_date": "YYYY-MM-DD HH-MM-SS"
    }
  ]
}
```

### GET /students/{email}/firms/{date}
Se presente, restituisce firma e controfirma di un dato studente alla data specificata
```json
{
  "status": 200,
  "data": {
    "name": "Mario",
    "surname": "Rossi",
    "email": "rossi.mario@itismeucci.com",
    "date": "YYYY-MM-DD",
    "odg": "Longimiranza del nanometro",
    "firm_date": "YYYY-MM-DD HH:MM:SS",
    "counterfirm_date": "YYYY-MM-DD HH:MM:SS"
  }
}
```
Se lo studente non ha controfirmato il campo *counterfirm* conterrà *null*
```json
{
  "status": 200,
  "data": {
    "name": "Mario",
    "surname": "Rossi",
    "email": "rossi.mario@itismeucci.com",
    "date": "YYYY-MM-DD",
    "odg": "Longimiranza del nanometro",
    "firm_date": "YYYY-MM-DD HH:MM:SS",
    "counterfirm_date": null
  }
}
```
Se lo studente non ha firmato restituira un s.c. 404
```json
{
  "status": 404,
  "error": "Firm not found"
}
```
### POST /students/{email}/firm
Inserisce una firma alla data odierna per il dato studente
>TODO: documentation

### POST /students/{email}/counterfirm
>TODO: documentation

### DELETE /students/{email}/firm
>NOT IMPLEMENTED

### DELETE /students/{email}/firm
>NOT IMPLEMENTED

### PUT /students/{email}/firm
>NOT IMPLEMENTED

### PUT /students/{email}/counterfirm
>NOT IMPLEMENTED
